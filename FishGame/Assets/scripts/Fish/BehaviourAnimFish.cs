﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourAnimFish : MonoBehaviour 
{
	public enum BehaviourFish { stay, sleep, walk, lookAtCamera, dead, fight, eatting};
    public enum EmotionalFish { quiet, tired };
	public BehaviourFish ActiveState=BehaviourFish.stay;
    public EmotionalFish EmotionState = EmotionalFish.quiet;

    public int Health = 100;
    public int Strength = 10;
	public float maxSpeed = 10.0f;
    

	public Animator FishAnimator;
	private BehaviourFish OldActiveState;

	#region BehaviourFish
	void BehaviourFishStay()
	{
		Debug.Log ("The fish stay");
		FishAnimator.SetTrigger ("stay_v1");
		GetComponent<unitMove> ().stateOn=false;
	}
	void BehaviourFishWalk()
	{
		Debug.Log ("The fish walk");
		FishAnimator.SetTrigger ("walk_v1");
		GetComponent<unitMove> ().stateOn=true;
	}
	void BehaviourFishSleep()
	{
		Debug.Log ("The fish sleep");

	}
	void BehaviourFishLookAtCamera()
	{
		Debug.Log ("The fish lookAtCamera");
	}
	void BehaviourFishDead()
	{
		Debug.Log ("the oldfish stinks");
	}
	void BehaviourFishFight()
	{
		Debug.Log ("the oldfish agree");
	}
	void BehaviourOurFish()
	{		
		switch (ActiveState) 
		{
		case BehaviourFish.stay:
			{					
				BehaviourFishStay ();
			}
			break;
		case BehaviourFish.walk:
			{				
				BehaviourFishWalk ();
			}
			break;
		case BehaviourFish.sleep:
			{				
				BehaviourFishSleep ();
			}
			break;
		case BehaviourFish.lookAtCamera:
			{
				BehaviourFishLookAtCamera ();
			}
			break; 
		case BehaviourFish.dead:
			{
				BehaviourFishDead();
			}
			break;
		case BehaviourFish.fight:
			{
				BehaviourFishFight();
			}
			break;
		default:
			{
				BehaviourFishStay ();
			}
			break;
		}
	}
	void ChangeBehavior()
	{
		if (OldActiveState!=ActiveState)
		{		
			BehaviourOurFish ();
			// написать общий метод включения тригера 
			OldActiveState = ActiveState;
		}
		Debug.Log ("was start animation for " + ActiveState);
	}
	#endregion




	// Use this for initialization
	void Start () 
	{		
		OldActiveState = BehaviourFish.stay;
	}
	
	// Update is called once per frame
	void Update () 
	{
		ChangeBehavior ();
	}
}
