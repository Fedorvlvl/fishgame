﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * ноды - координаты куда рыба приплывет. вокруг некоторых объектов будут создаваться пути чтобы рыба плыла в нору например.
 * параметры ноды будут сообщать рыбе какието данные, например что следует снизить скорость (предпоследняя нода  SlowDown ), или поменять путь. 
 * для автоматических какихто действий рыбы при достижении ноды. 
 * при достижении последней ноды автоматически будет приниматься вариативное решение что делать рыбе дальше. 
 * нода с тэгом враг будет сообщать что нужно укусить.
 * 
 * */
public class NodeParameters : MonoBehaviour 
{
	public enum StateThisNode {free=0,SlowDown=1,Enemy=2, LastNoda=3};

	public StateThisNode StateNow;

	public bool SpaceK=false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Space) && SpaceK == true) 
		{
			Debug.Log ("information from NodePlace... " + StateNow);
		}
	}
}
