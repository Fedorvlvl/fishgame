﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathGoConsructor : MonoBehaviour
{
    public GameObject[] AquariumSize;
    public int countNodes = 8;
    public GameObject Node;
    float x1, y1, z1, x2, y2, z2;
    float x, y, z;
    void findSpaseAquariumForFish()
    {
       foreach(GameObject sizeXYZ in AquariumSize)
        {
            if (sizeXYZ.transform.position.x!=0)
            {
                x = sizeXYZ.transform.position.x;
                if (x1 == 0)
                {
                    x1 = x;
                }
                else x2 = x;
            }
            if (sizeXYZ.transform.position.y != 0)
            {
                y = sizeXYZ.transform.position.y;
                if (y1 == 0)
                {
                    y1 = y;
                }
                else y2 = y;
            }
            if (sizeXYZ.transform.position.z != 0)
            {
                z = sizeXYZ.transform.position.z;
                if (z1 == 0)
                {
                    z1 = z;
                }
                else z2 = z;
            }
        }
       Debug.Log("the group coordinats for Aquariums wall: "+"x1="+x1+" x2="+x2+" y1="+y1+" y2="+y2+" z1="+z1+" z2="+z2);
    }
    void CreateNodesForFish()
    {
        findSpaseAquariumForFish();
        for (int i = 0; i < countNodes; i++)
        {
            Instantiate(Node,new Vector3(Random.Range(x1, x2), Random.Range(y1, y2), Random.Range(z1, z2)),
                        Quaternion.identity).transform.parent=gameObject.transform;            
        }
    }


	// Use this for initialization
	void Start () {        
        CreateNodesForFish();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Q))
        {
            CreateNodesForFish();
        }
	}
}
