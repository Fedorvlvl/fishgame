﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    float MIN_X = -24;
    float MAX_X = 24;
    
    float MIN_Y = -3.8f;
    float MAX_Y = 10;

    [SerializeField] float _dragSpeed = 2.0f;
    Vector3 _dragOrigin;

    void LateUpdate()
    {
        if (Input.GetMouseButtonDown(0)) {
            _dragOrigin = Input.mousePosition;
            return;
        }
        if (!Input.GetMouseButton(0)) { return; }

        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - _dragOrigin);
        Vector3 move = new Vector3(pos.x * _dragSpeed, pos.y * _dragSpeed, 0);
 
        transform.Translate(move, Space.World);

        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, MIN_X, MAX_X),
            Mathf.Clamp(transform.position.y, MIN_Y, MAX_Y),
            transform.position.z);
    }
}