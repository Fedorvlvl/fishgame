﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 The base for drive Unit
 if this unit have "stateOn" -> 1)finded the Path (It look for next position node "GetPathNode()" ) 
 */

// ДОПИЛИТЬ ПОВОРОТ ХВОСТА

public class unitMove : MonoBehaviour
{
	public bool stateOn=false; 

	public GameObject path;
	GameObject pathGo;
	public int pathNodeIndex=0; // the number of node for attak or fallow
	public int IndexCurrentNode=0; // the number of node that left or current 
	Transform transformTargetNode=null;
    Animator FishAnim;
    int layerindex; // for layer into AnimatorFish rotate tail
    //int IndexRL;
	public float speed=10.0f;
    float TailControllerX;

	void foolproof_pathNodeIndex()
	{
		if (pathNodeIndex < 0)
		{
			pathNodeIndex = 0;
		}
		if (pathNodeIndex >= pathGo.transform.childCount) 
		{
			pathNodeIndex = pathGo.transform.childCount - 1;
		}
	}

	void TakeNode()
	{
		IndexCurrentNode = pathNodeIndex;
	}
	void GetPathNode()
	{			
		transformTargetNode = pathGo.transform.GetChild (pathNodeIndex);
	}

	void MoveToNode()
	{		
		Vector3 dir=transformTargetNode.position-this.transform.localPosition;
        float distThisFrame = speed * Time.deltaTime;
        if (dir.magnitude <= distThisFrame)
        {
            //we reached this node
            transformTargetNode = null;
            TakeNode();
            FishAnim.SetTrigger ("walk_v1");
        }
        else
        {
			// Move torwards node
            transform.Translate(dir.normalized * distThisFrame, Space.World);
            Quaternion targetRotation = Quaternion.LookRotation(dir);
            //speed rotate for nextNode
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRotation, Time.deltaTime * 1.0f);
            //turnOn animation for Tail 
            if (Quaternion.LookRotation(dir).x < 0)
            {
                 TailControllerX = Quaternion.LookRotation(dir).x + this.transform.rotation.x;
            }
            else {  TailControllerX = Quaternion.LookRotation(dir).x - this.transform.rotation.x; }
            
            
            var TailControllerY = Quaternion.LookRotation(dir).z - this.transform.rotation.z;
            Debug.Log( "lookX =" + Quaternion.LookRotation(dir).x +"   thisX ="+ this.transform.rotation.x+ "     TailControllerX = " + TailControllerX );
            //Debug.Log("TailControllerY = " + TailControllerY);
            //ПОВОРОТ ХВОСТА
            FishAnim.SetLayerWeight(layerindex, 1.0f);
            FishAnim.SetFloat("Turn_Top_Down", TailControllerX * (10f));
            //FishAnim.SetFloat("Turn_Left_Right", TailControllerY * 5f);

        }
	}

	void UnitBechaiviour()
	{		
		if (stateOn)
		{
			GetPathNode (); // we riched nomber of next node
			MoveToNode ();
            if (pathNodeIndex == IndexCurrentNode)
            {
                pathNodeIndex++;
            }
            if (pathNodeIndex == (pathGo.transform.childCount - 1))
            {
                pathNodeIndex = 0;
            }
		}        
	}
	void Start () 
	{
        FishAnim = GetComponent<BehaviourAnimFish>().FishAnimator;
        layerindex = FishAnim.GetLayerIndex("Turn_TDLR"); 
		pathGo = path;// set the path		
	}
	// Update is called once per frame
	void Update () 
	{		
		foolproof_pathNodeIndex ();
		UnitBechaiviour();       
	}
/// </summary>


}
