﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FromAtoB : MonoBehaviour
{
    public bool reset = false;
    [Header("----SETUP----")]
    public AnimationCurve changePos;
    public Transform[] pointsAB;
    public Vector3 offset;
    public float timeAnimation = 3f;
    [Header ("randomOffset:")]
    [Header("-----------")]
    public bool IsRandomOfeset;
    public Vector2 x;
    public Vector2 y;
    public Vector2 z;
    [Header("-----------")]
    
    public bool deactivate = false;
    public float timerBeforeDeactivate = 1f;


    private float startTime = 0f;

    private void Awake()
    {        
        transform.position = pointsAB[0].position;
        if (IsRandomOfeset)
        {
            offset = new Vector3(Random.Range(x.x, x.y), Random.Range(y.x, y.y), Random.Range(z.x, z.y));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(pointsAB!=null)
        {
            startTime += Time.deltaTime;
            if (startTime >= timeAnimation)
            {
                
                startTime = timeAnimation;
                transform.position = pointsAB[1].position;
                if (deactivate && transform.position == pointsAB[1].position)
                {
                    StartCoroutine(Deactivator(timerBeforeDeactivate));
                }
            }
            float timeRatio = startTime / timeAnimation;
            
            Vector3 positionOffset = changePos.Evaluate(timeRatio)*offset;
            transform.position = Vector3.Lerp(pointsAB[0].position,
                pointsAB[1].position,
                timeRatio) + positionOffset;
        }

        if (reset)
        {
            reset = false;
            startTime = 0f;
            
            
            if (IsRandomOfeset)
            {
                offset = new Vector3(Random.Range(x.x, x.y), Random.Range(y.x, y.y), Random.Range(z.x, z.y));
            }
        }

    }

    IEnumerator Deactivator(float timerBeforeDeactivate)
    {
        yield return new WaitForSeconds(timerBeforeDeactivate);
        gameObject.SetActive(false);
    }
}
